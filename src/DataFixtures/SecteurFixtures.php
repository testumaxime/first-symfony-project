<?php

namespace App\DataFixtures;
use App\Entity\Secteur;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SecteurFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $s1 = new Secteur();
        $s1->setLibelle('S1');
        $manager->persist($s1);
        $manager->flush();
    }
}
