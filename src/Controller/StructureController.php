<?php

namespace App\Controller;

use App\Entity\Structure;
use App\Entity\StructureSearch;
use App\Form\StructureType;
use App\Form\StructureSearchType;
use App\Repository\StructureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\String\Slugger\AsciiSlugger;

#[Route('/structure')]
class StructureController extends AbstractController
{
    #[Route('/', name: 'app_structure_index', methods: ['GET'])]
    public function index(StructureRepository $structureRepository): Response
    {
        return $this->render('structure/index.html.twig', [
            'structures' => $structureRepository->findAll(),
        ]);
    }

    #[Route('/search', name: 'app_structure_search', methods: ['GET', 'POST'])]
    public function search(Request $request, StructureRepository $structureRepository): Response
    {
        $structure = new StructureSearch();
        $form = $this->createForm(StructureSearchType::class, $structure);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $res = $structureRepository->findByNomAndAsso($structure->getNom(), $structure->isEstasso());
            if(isset($res[0])){
                return $this->redirectToRoute('app_structure_show', ['id'=>$res[0]->getId()], Response::HTTP_SEE_OTHER);
            }else{
                return $this->renderForm('structure/search.html.twig', [
                    'structure' => $structure,
                    'formulaire' => $form,
                    'no_result'=>true,
                ]);
            }
        }

        return $this->renderForm('structure/search.html.twig', [
            'structure' => $structure,
            'formulaire' => $form,
            'no_result'=>false,
        ]);
    }

    #[Route('/new', name: 'app_structure_new', methods: ['GET', 'POST'])]
    public function new(Request $request, StructureRepository $structureRepository): Response
    {
        $structure = new Structure();
        $form = $this->createForm(StructureType::class, $structure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('url_logo')->getData();

            if ($logo) {
                $slugger = new AsciiSlugger();
                $dtNow = new \DateTime();
                $originalFilename = pathinfo($logo->getClientOriginalName(), PATHINFO_FILENAME);

                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.$dtNow->getTimestamp().'.'.$logo->getClientOriginalExtension();

                try {
                    $logo->move(
                        $this->getParameter('img_uploaded_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }

                $structure->setUrlLogo($newFilename);
            }

            $structureRepository->save($structure, true);

            return $this->redirectToRoute('app_structure_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('structure/new.html.twig', [
            'structure' => $structure,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_structure_show', methods: ['GET'])]
    public function show(Structure $structure): Response
    {
        return $this->render('structure/show.html.twig', [
            'structure' => $structure,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_structure_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Structure $structure, StructureRepository $structureRepository): Response
    {
        $form = $this->createForm(StructureType::class, $structure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $structureRepository->save($structure, true);

            return $this->redirectToRoute('app_structure_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('structure/edit.html.twig', [
            'structure' => $structure,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_structure_delete', methods: ['POST'])]
    public function delete(Request $request, Structure $structure, StructureRepository $structureRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$structure->getId(), $request->request->get('_token'))) {
            $structureRepository->remove($structure, true);
        }

        return $this->redirectToRoute('app_structure_index', [], Response::HTTP_SEE_OTHER);
    }
}
