<?php

namespace App\Controller;

use App\Operations\Operation;
use App\Service\OperationsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OperationsController extends AbstractController
{
    public function yamlHome(): Response
    {
        return $this->render('operations/index.html.twig', [
            'controller_name' => 'OperationsController',
        ]);
    }

    public function calculYaml(float $val1, float $val2, string $op): Response
    {
        return new Response('Calcul Yaml : val1='.$val1.', val2='.$val2.', op='.$op);
    }

    #[Route(path: '/ope/annot/{val1}/{op}/{val2}', name: 'ope_annot_calcul', requirements: ['val1' => '-?\d+\.?\d*', 'val2' => '-?\d+\.?\d*', 'op' => 'plus|moins|mult|div'])]
    public function calculAnnot(float $val2, float $val1, string $op): Response
    {
        //return new Response('Calcul Annot : val1='.$val1.', val2='.$val2.', op='.$op);
        return new Response($val1.' '.$op.' '.$val2.' = '.$this->calcul($val1,$val2,$op));
    }

    public function calcul(float $nb1, float $nb2, string $op) : string {
        switch ($op) {
            case 'plus' :
                $res=$nb1+$nb2;
                break;
            case 'moins' :
                $res=$nb1-$nb2;
                break;
            case 'mult' :
                $res=$nb1*$nb2;
                break;
            case 'div' :
                if ($nb2==0) {
                    $res='Division par zéro impossible';
                }
                else {
                    $res=$nb1/$nb2;
                }
                break;
            default:
                $res='Mauvais opérateur';
                break;
        }
        return $res;
    }

    #[Route(path: '/opeservice/annot/{val1}/{op}/{val2}', name: 'ope_annot_calcul_service', requirements: ['val1' => '-?\d+\.?\d*', 'val2' => '-?\d+\.?\d*', 'op' => 'plus|moins|mult|div'])]
    public function calculAnnotService(float $val2, float $val1, string $op,
                                       OperationsService $opService): Response
    {
        return new Response('Résultat Annot avec service: '.$opService->calcul($val1,$val2,$op));
    }

    #[Route(path: '/opetwig/annot/{val1}/{op}/{val2}', name: 'ope_annot_calcul_twig', requirements: ['val1' => '-?\d+\.?\d*', 'val2' => '-?\d+\.?\d*', 'op' => 'plus|moins|mult|div'])]
    public function calculTwig(float $val1, string $op, float $val2) : Response
    {
        return $this->render('operations/operation.html.twig', [
            'v1' => $val1, 'v2' => $val2, 'operateur' => $op
        ]);
    }

    #[Route(path: '/opeform/annot', name: 'ope_annot_form')]
    public function form(Request $request) : Response
    {
        $operation = new Operation();

        $form = $this->createFormBuilder($operation,array())
            ->add('val1', NumberType::class)
            ->add('op', ChoiceType::class,
                array('choices'=>array('+'=>'plus','-'=>'moins','*'=>'mult','/'=>'div')))
            ->add('val2', NumberType::class)
            ->add('calculer', SubmitType::class, array('label' => 'Calculer'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $operation = $form->getData();
            return $this->redirectToRoute('ope_annot_calcul',
                array('val1'=>$operation->getVal1(),
                    'op'=>$operation->getOp(),
                    'val2'=>$operation->getVal2()));
        }

        return $this->render('operations/form.operation.html.twig',
            array('formulaire' => $form->createView()));
    }
}
